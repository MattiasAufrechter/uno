module.exports = (sequelize, Sequelize) => {
    const Party = sequelize.define("parties", {
        isLaunched: {
            type: Sequelize.BOOLEAN
        },
        color: {
            type: Sequelize.STRING
        },
        isPlaying: {
            type: Sequelize.JSON
        }
    });

    return Party;
};