module.exports = (sequelize, Sequelize) => {
    const Card = sequelize.define("cards", {
        color: {
            type: Sequelize.STRING
        },
        value: {
            type: Sequelize.STRING
        },
        score: {
            type: Sequelize.INTEGER
        }
    }, {
        generateAssociationMethods: true
    });

    return Card;
};