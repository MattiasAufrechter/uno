const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host: config.HOST,
        dialect: config.dialect,
        operatorsAliases: 0,

        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        }
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.card = require("../models/card.model.js")(sequelize, Sequelize);
db.party = require("../models/party.model.js")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
    through: "user_roles",
    foreignKey: "roleId",
    otherKey: "userId"
});

db.user.belongsToMany(db.role, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
});

db.pioche = db.party.hasMany(db.card, { as: 'pioche', foreignKey: 'piocheId' });
db.defausse = db.party.hasMany(db.card, { as: 'defausse', foreignKey: 'defausseId' });
db.hand = db.user.hasMany(db.card, {as: 'hand', foreignKey: 'handId' });

db.card.belongsTo(db.party, { as: 'pioche' });
db.card.belongsTo(db.party, { as: 'defausse' });
db.card.belongsTo(db.user, {as: 'hand'});

db.party.users = db.party.hasMany(db.user);
db.user.belongsTo(db.party);

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;