module.exports = (param) => {
  return function (req, res, next) {
    let actualHeaderAccept = res.req.headers.accept;
    if (!param.formats.includes(actualHeaderAccept)) {
      res.render = () => { res.sendStatus(406) };
    } else {
      res.render = (result) => {
        res.format({
          "application/json": () => res.json(result),
          "application/hal+json": () => res.send(JSON.stringify(result) + JSON.stringify(req.hateoas)),
          "text/plain": () => {
            res.setHeader("Content-Type", "text/plain");
            if (!Array.isArray(result)) result = [result];
            res.send(result.map((u) => JSON.stringify(u)).join("\n"));
          },
          "text/csv": () => {
            res.setHeader("Content-Type", "result/csv");
            if (!Array.isArray(result)) result = [result];
            res.write(Object.keys(result[0].dataValues).join(";") + "\n");
            res.write(
              result.map((u) => Object.values(u.dataValues).join(";")).join("\n")
            );
            res.end();
          },
          default: () => res.json(result),
        })
      };
    }
    next()
  }
}