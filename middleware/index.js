const authJwt = require("./authJwt");
const verifySignUp = require("./verifySignUp");
const negociate_format = require("./negociate_format");
const apiVersions = require("./apiVersions");
const negociate_trad = require("./negociate_trad");
const useHATEOAS = require("./useHATEOAS");

module.exports = {
    authJwt,
    verifySignUp,
    negociate_format,
    apiVersions,
    negociate_trad,
    useHATEOAS
};
