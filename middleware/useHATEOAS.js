module.exports = (param) => {
    return function (req, res, next) {
        let users = [
            {
                "id": 1,
                "name": "Paul ROBERT",
                "password": "PIOJSAhsiauhAhhsbjau"
            },
            {
                "id": 2,
                "name": "Colin Alaska",
                "password": "POUAOIDHIUZagdyagdAIYGDAIGgy"
            }
        ];
        console.log(users);
        let defaultType = "GET";
        let defaultHref = `${req.protocol}://${req.get("host")}${req.baseUrl
            }/users`;
        let resultat = {
            _links: {}
        }
        if (param.links) {
            for (const [key1, value1] of Object.entries(param.links)) {
                let newType = defaultType;
                let newhref = defaultHref;
                for (const [key, value] of Object.entries(value1)) {
                    if (key == "type") {
                        newType = value;
                    }
                    if (key == "href" || key == "path") {
                        newhref += value;
                    }
                }
                resultat._links[key1] = {
                    type: newType,
                    href: newhref
                };
            }
        }
        req.hateoas = resultat;
        // console.log(resultat);
        next()
    }
}