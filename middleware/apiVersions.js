module.exports = (param, defaultVersion) => {
  return function (req, res, next) {
    if (req.headers["accept-version"]) {
      req.api_version = req.headers["accept-version"];
    } else if (req.query["_apiVersion"]) {
      req.api_version = req.query["_apiVersion"];
    } else {
      if(defaultVersion){
        req.api_version = defaultVersion;
      }else{
        res.sendStatus(404);
      }
    }
    let require = param[req.api_version];
    return require(req, res, next);
  }
}