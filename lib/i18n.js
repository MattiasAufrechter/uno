const i18next = require("i18next");

i18next.init({
  lng: "fr",
  fallbackLng: "fr",
  resources: {
    en: {
      translation: {
        "Vous avez déjà une partie en cours.": "You already have a party linked to your account.",
        "Contenu utilisateur." : "User Content.",
        "Contenu administrateur." : "Admin Content.",
        "Contenu modérateur." : "Moderator Content.",
        "Contenu publique." : "Public Content.",
        "Mot de passe incorrect!" : "Invalid Password!",
        "L'utilisateur est introuvable." : "User Not found.",
        "L'utilisateur a bien été enregistré!" : "User was registered successfully!",
        "Vous devez être au moins 2 pour lancer la partie." : "You need to be at least 2 to launch the party.",
        "La partie a déjà été lancée." : "Party already launched.",
        "Vous n'êtes pas dans une partie." : "You are not in a party.",
        "Le nombre maximum de joueurs a été atteint." : "Maximum number of users reached for this party.",
        "Aucune partie trouvée." : "Party not found.",
        "Pas de contre uno jouable" : "No counter playable",
        "Contre uno bien effectué" : "Counter done",
        "La carte a été retirée de la pioche et ajoutée à la hand du userd" : "The card has been removed from the draw pile and added to the userId's hand",
        "Uno pas réalisable" : "Uno impossible",
        "Uno bien effectué" : "Uno done",
        "It's all good man!" : "Vous avez bien pioché",
        "Impossible de retirer une carte de la pioche" : "Unable to remove a card from the draw pile",
        "Pas de couleur" : "No color",
        "Impossible de retirer une carte de la main" : "You can't remove a card from hand",
        "Le changement de couleur a été effectué" : "The color change has been made",
        "Le changement de sens a bien été effectué" : "The change of direction has been made",
        "Utilisateur non mis à jour." : "User not updated",
        "Vous avez déjà une partie en cours." : "You already have a game in progress",
      },
    },
  },
});

module.exports = i18next;
