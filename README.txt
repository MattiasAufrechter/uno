Setup pour tout tester :

Se rendre en POST sur http://localhost:3000/api/auth/signup
Avec comme body :
{
    "username": "user1",
    "email": "user1@user.user",
    "password": "user1",
    "roles": ["user", "user"] 
}

Pour se connecter avec ce compte :

Se rendre en POST sur http://localhost:3000/api/auth/signin
Avec comme body : 
{
    "username": "user1",
    "password": "user1"
}

Gardez l'accessToken de côté


Créez un deuxième compte en répétant les étapes précédentes en remplaçant user1 par user2



Pour créer une partie :

Se rendre en GET sur http://localhost:3000/api/game/createParty
Ajoutez un header :
x-access-token : [Le token récupéré dans la connexion avec le premier compte]


Pour rejoindre une partie :
Se rendre en GET sur http://localhost:3000/api/game/join/1
1 étant l'id de la partie que l'on veut créer
Ajoutez un header :
x-access-token : [Le token récupéré dans la connexion avec le deuxième compte]

Pour lancer la partie :
Se rendre en GET sur http://localhost:3000/api/game/launchParty
Ajoutez un header :
x-access-token : [Le token récupéré dans la connexion avec le deuxième compte]


Chaque user qui a rejoint la partie a maintenant 7 cartes dans sa main
C'est le joueur qui a créé la partie qui commence


Pour jouer une carte :
Se rendre en GET sur http://localhost:3000/api/game/play/0
0 étant la première carte de la main (vous pouvez changer le numéro)
Ajoutez le paramètre colorchoosed si votre carte est noire
Ajoutez un header :
x-access-token : [Le token récupéré dans la connexion du joueur qui doit jouer]

Pour piocher une carte :
Se rendre en GET sur http://localhost:3000/api/game/pioche
Ajoutez un header :
x-access-token : [Le token récupéré dans la connexion du joueur qui doit jouer]

Pour dire UNO :
Se rendre en GET sur http://localhost:3000/api/game/uno
Ajoutez un header :
x-access-token : [Le token récupéré dans la connexion ]

Pour dire CONTRE-UNO :
Se rendre en GET sur http://localhost:3000/api/game/contre_uno
Ajoutez un header :
x-access-token : [Le token récupéré dans la connexion ]




Pour tester les middlewares :

1 - Middleware de format :
Se rendre en GET sur http://localhost:3000/api/game/parties
Ajouter un header :
Accept : text/csv (ou text/plain ou application/json)

Le résultat sera sous forme de csv !



2 - Middleware de traduction:
Avec son token, se rendre sur la route GET http://localhost:3000/api/game/createParty
puis lancer la route deux fois afin d'obtenir le message d'erreur "Vous avez déjà une partie en cours."

Ensuite ajouter le header:
Accept-Language : en

Le résultat sera en anglais !

3 - Middleware de version :
Se rendre en GET sur http://localhost:3000/api/game/parties
Ajouter un header :
accept-version : v1

V2 est la version par défaut, cette route renvoie une 404 sur la v1 et toutes les parties sur la v2 !
