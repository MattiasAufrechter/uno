const Sequelize = require('sequelize');
const db = require("../models");
const Op = Sequelize.Op;
const Card = db.card
const Party = db.party
const User = db.user
const Pioche = db.pioche
const Defausse = db.defausse
const Hand = db.hand

// Mélange les cartes aléatoirement
function shuffleCards(cards) {
  for (var i = cards.length - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var temp = cards[i];
    cards[i] = cards[j];
    cards[j] = temp;
  }
}

// Vérifie si l'utilisateur n'est pas déjà lié à une party
async function checkUserParty(user, res) {
  if (await user.getParty()) {
    res.status(409).render(res.t("Vous avez déjà une partie en cours."));
    return true;
  } else {
    return false;
  }
}

async function isNext(party, user){
  let index = party.users.map(e => e.id).indexOf(user.id);
    if(index === party.users.length -1){
      party.isPlaying = party.users[0]
    }
    else{
      party.isPlaying = party.users[index + 1]
    }
  Promise.all([party.save()]).then(() => {
    console.log('Changement de joueur');
  });
}

async function Reverse(party) {
  await party.update({ users: party.users.reverse() }, {
  });
  Promise.all([party.save()]).then(() => {
    console.log(JSON.stringify(party.users));
  });
}

async function colorChange(color, party){
  await party.update({ color: color }, {
  });
  Promise.all([party.save()]).then(() => {
    console.log('Le changement de couleur a été effectué');
  });
}

// async function CheckDefausseCard(user, res){
//   const user = await User.findByPk(req.userId);
//   if (await checkUserParty(user, res)) {
//     return;
//   };

// }

async function Endgame(party){
  await Card.destroy({ where: { [Op.or]: [{ piocheId: party.id }, { defausseId: party.id }, { partyId: party.id }, {[Op.and]: [{ piocheId: null }, { defausseId: null }, { handId: null }] }] } });
  party.users.forEach(async user =>{
    await Card.destroy({ where: { handId: user.id } });
    user.partyId = null
    Promise.all([user.save()]).then(() => {
      console.log("le user a quitté la partie");
    });
  })
  await party.destroy()
}

exports.createParty = async (req, res) => {
  const user = await User.findByPk(req.userId);
  if (await checkUserParty(user, res)) {
    return;
  };
  const colors = ["blue", "green", "yellow", "red"];
  const allCard = () => {
    let cards = [];
    for (const color of colors) {
      // Création de la carte 0
      cards.push({
        color: color,
        value: 0,
        score: 0
      });
      // Création des autres cartes numérotés (en double)
      for (let i = 1; i <= 9; i++) {
        for (let j = 0; j < 2; j++) {
          cards.push({
            color: color,
            value: i,
            score: i
          });
        }
      }
      // Création des cartes spéciales 
      for (let i = 0; i < 2; i++) {
        cards.push({
          color: color,
          value: "+2",
          score: 20
        })
        cards.push({
          color: color,
          value: "Reverse",
          score: 20
        })
        cards.push({
          color: color,
          value: "Skip",
          score: 20
        })
      }
      cards.push({
        color: "black",
        value: "Color change",
        score: 50
      })
      cards.push({
        color: "black",
        value: "+4",
        score: 50
      })
      // Carte non incluse dans notre UNO
      /*
      for (let i = 0; i <= 3; i++){
        cards.push({
          color: "black",
          value: "Customisable card",
          score: 0
        });
      }
      */
    }
    // Pour l'instant pas inclus dans le jeu, à méditer
    /*
      cards.push({
        color: "black",
        value: "Shuffle hands",
        score: 40
      });
    */

    shuffleCards(cards)

    return cards
  }

  let pioche = allCard()
  let defausse = [pioche[pioche.length - 1]]
  pioche.splice(0,1)
  console.log(JSON.stringify(defausse))
  if(defausse[0].color == "black"){
    defausse = pioche[pioche.length - 1]
    pioche.splice(0,1)
  }

  const party = await Party.create({
    isFinished: false,
    winner: null,
    pioche: pioche,
    defausse: defausse
  }, {
    include: [Pioche, Defausse]
  })
  await party.addUser(user);
  res.status(200).render(`${JSON.stringify(party.pioche)}, ${JSON.stringify(party.defausse)}`);
}

exports.joinParty = async (req, res) => {
  const user = await User.findByPk(req.userId);
  const party = await Party.findByPk(req.params.partyId);
  if (!party) {
    res.status(404).render(res.t("Aucune partie trouvée."));
    return;
  }

  if (await checkUserParty(user, res)) {
    return;
  };

  if (await party.countUsers() >= 10) {
    res.status(403).render(res.t("Le nombre maximum de joueurs a été atteint."));
    return;
  }

  await party.addUser(user);
  res.status(200).render(party);
}

exports.launchParty = async (req, res) => {
  const user = await User.findByPk(req.userId);
  const party = await user.getParty();
  if (!party) {
    res.status(404).render(res.t("Vous n'êtes pas dans une partie."));
    return;
  }
  const pioche = await party.getPioche();
  const users = await party.getUsers();

  // Si la partie est déjà lancée
  if (party.isLaunched) {
    res.status(403).render(res.t("La partie a déjà été lancée."
    ));
    return;
  }

  // S'il y a moins de 2 joueurs
  if (users.length < 2) {
    res.status(403).render(res.t("Vous devez être au moins 2 pour lancer la partie."
    ));
    return;
  }

  // Distribution des cartes
  for (let userParty in users) {
    // Permet de remettre les hands des utilisateurs à 0
    // console.log(await users[userParty].setHand([]));
    for (i = 0; i < 7; i++) {
      let card = pioche.shift();
      await users[userParty].addHand(card);
      await party.removePioche(card);
    }
  }
  await party.update({ pioche, isLaunched: true, isPlaying: users[0] });

  res.status(200).render(party);
}

exports.seeDefausse = async (req, res) => {

  const user = await User.findOne({
    where: {
      id: req.userId,
    },
    include: [
      {
        model: Card,
        as: 'hand'
      }
    ]
  });

  const party = await Party.findOne({
    where: {
      id: user.partyId
    },
    include: [
      {
        model: Card,
        as: 'defausse'
      }
    ]
  });
  res.status(200).render(`${JSON.stringify(party.defausse)}`)
}

exports.getAllCardsFromUser = async (req, res) => {
  res.status(200).render(await User.findOne({ where: { id: req.userId }, include: [{ model: Card, as: 'hand' }] }));
}

exports.getAllParties = async (req, res) => {
  res.status(200).render(await Party.findAll({ include: [User, Pioche, Defausse] }));
}

exports.getAllCards = async (req, res) => {
  res.status(200).send(await Card.findAll());
}

exports.getAllCards = async (req, res) => {
  res.status(200).send(await Card.findAll());
}

exports.getAllUsers = async (req, res) => {
  res.status(200).render(await User.findAll({ include: [Hand] }));
}

exports.getAllCardsFromUser = async (req, res) => {
  res.status(200).render(await User.findOne({ where: { id: req.userId }, include: [{ model: Card, as: 'hand' }] }));
}

exports.play = async (req, res) => {
  const user = await User.findOne({
    where: {
      id: req.userId
    },
    include: [
      {
        model: Card,
        as: 'hand'
      }
    ]
  });

  const party = await Party.findOne({
    where: {
      id: user.partyId
    },
    include: [
      {
        model: Card,
        as: 'defausse'
      },
      {
        model: Card,
        as: 'pioche'
      },
      User
    ]
  });

  if (!party || user.hand.length === 0) {
    // vérifie que la party existe et qu'il y a au moins une carte dans la hand du user
    res.status(400).render(res.t('Impossible de retirer une carte de la hand'));
    return;
  }
  let NextPlayer = null
  let index = party.users.map(e => e.id).indexOf(user.id);
  if(index === party.users.length -1){
      NextPlayer = party.users[0]
    }
    else{
      NextPlayer = party.users[index + 1]
    }
  const lastDefausseCard = party.defausse[0]
  let NextPlayerUser = await User.findByPk(NextPlayer.id, {include:[{
    model: Card,
    as: 'hand'
  }]})
  let partyColor = null;
  if (lastDefausseCard.color == "black") {
    partyColor = party.color
  } else{
    partyColor = lastDefausseCard.color
  }
  const card = user.hand[req.params.id]
  const canPlay = async () => {
    console.log(card.dataValues.color)
    console.log(card.dataValues.value)
    console.log(lastDefausseCard.color)
    console.log(lastDefausseCard.value)
    if(card.dataValues.color != partyColor && card.dataValues.value != lastDefausseCard.value && card.dataValues.color != "black"){
      return false
    }
    if(card.dataValues.color === "black"){
      if(req.param('colorchoosed') == null){
        console.log("Il manque la couleur en paramètre")
        return false
      }else if(req.param('colorchoosed') != "red" && req.param('colorchoosed') != "blue" && req.param('colorchoosed') != "green" && req.param('colorchoosed') != "yellow"  ){
        console.log("Mauvaise couleur")
        return false
      }
      else{
        await colorChange(req.param('colorchoosed'), party);
        if(card.dataValues.value == "+4"){
          for (let i = 0; i < 4; i++) {
            let card = party.pioche[0];
            party.pioche.splice(0,1)
            card.piocheId = null;
            card.handId = NextPlayerUser.id;
            NextPlayerUser.hand.push(card);
            Promise.all([card.save(), user.save(), party.save()]).then(() => {
              console.log('La carte a été retirée de la pioche et ajoutée à la hand du userId');
            });
          }
        }
      }
    }
    if(card.dataValues.value === "Reverse"){
      await Reverse(party)
    }
    if(card.dataValues.value === "Skip"){
      await isNext(party, user)
    }
    if(card.dataValues.value === "+2"){
      for (let i = 0; i < 2; i++) {
        let card = party.pioche[0]
        party.pioche.splice(0,1)
        card.piocheId = null;
        card.handId = NextPlayerUser.id;
        NextPlayerUser.hand.push(card);

        Promise.all([card.save(), user.save(), party.save()]).then(() => {
          console.log('La carte a été retirée de la pioche et ajoutée à la hand du userId');
        });
      }
      isNext(party, user)
    }
    return true
  }
  if(user.id == party.isPlaying.id){
    if(await canPlay() == true){
      user.hand.splice(req.params.id,1)
      card.handId = null;

      card.defausseId = party.id;
      party.defausse.push(card);
      user.uno = ( user.hand.length === 1 )

      if (user.hand.length === 0){
        Endgame(party)
        console.log("Partie terminée.")
      }

      Promise.all([card.save(), user.save(), party.save()]).then(() => {
        console.log('La carte a été retirée de la hand du user et ajoutée à la défausse');
        console.log(`A joué ${JSON.stringify(party.isPlaying)}`)
        isNext(party, user)
        console.log(`C'est a ${JSON.stringify(party.isPlaying)}`)
        res.status(200).send("c'est tout bon")
      });
    
      if (user.hand.length === 0){
        Endgame(party)
        console.log("Partie terminée.")
      }
    }
    else{
      res.status(200).send("tu ne peux pas jouer cette carte")
    }
  }else{
    res.status(200).send(`c'est pas a toi de jouer, mais a ${JSON.stringify(party.isPlaying)}`)
  }
}
exports.pioche = async (req, res) => {
  const user = await User.findOne({
    where: {
      id: req.userId
    },
    include: [
      {
        model: Card,
        as: 'hand'
      }
    ]
  });

  const party = await Party.findOne({
    where: {
      id: user.partyId
    },
    include: [
      {
        model: Card,
        as: 'pioche'
      }
    ]
  })

  if (!party || party.pioche.length === 0) {
    // vérifie que la party existe et qu'il y a au moins une carte dans la pioche
    res.status(400).render(res.t('Impossible de retirer une carte de la pioche'));
    return;
  }
  if(user.id == party.isPlaying.id){
    console.log("pioche avant" + party.pioche.length)
    console.log("hand avant" + user.hand.length)
    const card = party.pioche[0]
    party.pioche.splice(0,1)
    card.piocheId = null;
    card.handId = user.id;
    user.hand.push(card);

    console.log("pioche après" + party.pioche.length)
    console.log("hand après" + user.hand.length)
    console.log("user id " + user.id)
    Promise.all([card.save(), user.save(), party.save()]).then(() => {
      console.log('La carte a été retirée de la pioche et ajoutée à la hand du userId');
      res.status(200).send("It's all good man!")
    });
  } else{
    res.status(200).send(`c'est pas a toi de jouer, mais a ${JSON.stringify(party.isPlaying)}`)
  }
}

exports.uno = async (req, res) => {
  const user = await User.findOne({
    where: {
      id: req.userId
    }
  });
  if (user.uno === true) {
    user.uno = false
    Promise.all([user.save()]).then(() => {
      res.status(200).render(res.t("Uno bien effectué"))
    });
  } else {
    res.status(400).render(res.t("Uno pas réalisable"))
  }
}

exports.contre_uno = async (req, res) => {
  const user = await User.findOne({
    where: {
      id: req.userId
    },
    include: [
      {
        model: Card,
        as: 'hand'
      }
    ]
  });
  const party = await Party.findOne({
    where: {
      id: user.partyId
    },
    include: [
      {
        model: Card,
        as: 'pioche'
      }
    ]
  });
  const usersAttacked = await User.findAll({
    where: {
      partyId: party.id,
      uno: true
    },
    include: [
      {
        model: Card,
        as: 'hand'
      }
    ]
  });
  contre_uno = false
  usersAttacked.forEach(user => {
    if (user.uno === true) {
      user.uno = false
      contre_uno = true
      for (let i = 0; i < 2; i++) {
        let card = party.pioche[0]
        party.pioche.splice(0,1)
        card.piocheId = null;
        card.handId = user.id;
        user.hand.push(card);

        Promise.all([card.save(), user.save(), party.save()]).then(() => {
          console.log('La carte a été retirée de la pioche et ajoutée à la hand du userId');
        });
      }
    }
  });
  if(contre_uno === true){
    res.status(200).render(res.t("Contre uno bien effectué"))
  }else{
    res.status(200).render(res.t("Pas de contre uno jouable"))
  }
}