exports.allAccess = (req, res) => {
    res.status(200).render("Contenu publique.");
};

exports.userBoard = (req, res) => {
    res.status(200).render("Contenu utilisateur.");
};

exports.adminBoard = (req, res) => {
    res.status(200).render("Contenu administrateur.");
};

exports.moderatorBoard = (req, res) => {
    res.status(200).render("Contenu modérateur.");
};