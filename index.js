const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const middleware = require('./middleware')
const i18next = require("./lib/i18n");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Middleware pour gérer les formats
app.use(middleware.negociate_format({
formats: ["application/json", "application/hal+json", "text/plain", "text/csv", "*/*"]
}));

// Middleware de traduction
app.use(middleware.negociate_trad(i18next));

// Middleware de versionning
const defaultVersion = "v2"; 
app.use(
  "/api/game",
  middleware.apiVersions({
    v1: require("./routes/v1/game.routes.js"),
    v2: require("./routes/v2/game.routes.js")
  }, "v2")
);
app.use(
  "/api/auth",
  middleware.apiVersions({
    v1: require("./routes/v1/auth.routes.js"),
    v2: require("./routes/v2/auth.routes.js")
  }, "v2")
);
app.use(
  "/api/user",
  middleware.apiVersions({
    v1: require("./routes/v1/user.routes.js"),
    v2: require("./routes/v2/user.routes.js")
  }, "v2")
);

// simple route
app.get("/", (req, res) => {
  res.render(res.t("Venez jouer au UNO !"));
});

// Routes
/*require('./routes/auth.routes')(app);
require('./routes/user.routes')(app);
require('./routes/game.routes')(app);*/

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

const db = require("./models");
const Role = db.role;

// Permet de synchroniser la BDD à chaque reload
db.sequelize.sync({ force: true }).then(() => {
  console.log('Drop and Resync Db');
  initial();
});

function initial() {
  Role.create({
    id: 1,
    name: "user"
  });

  Role.create({
    id: 2,
    name: "moderator"
  });

  Role.create({
    id: 3,
    name: "admin"
  });
}

