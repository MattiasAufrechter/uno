const { authJwt } = require("../../middleware");
const controller = require("../../controllers/game.controller");
const { Router } = require("express");

const router = Router();

router.get("/createParty", [authJwt.verifyToken], controller.createParty);
router.get("/join/:partyId", [authJwt.verifyToken], controller.joinParty);
router.get("/launchParty", [authJwt.verifyToken], controller.launchParty);
router.get("/users", controller.getAllUsers);
router.get("/parties", controller.getAllParties);
router.get("/userCards", [authJwt.verifyToken], controller.getAllCardsFromUser);
router.get("/play/:id", [authJwt.verifyToken], controller.play);
router.get("/play/:id?color", [authJwt.verifyToken], controller.play);
router.get("/pioche", [authJwt.verifyToken], controller.pioche);
router.get("/uno", [authJwt.verifyToken], controller.uno);
router.get("/contre_uno", [authJwt.verifyToken], controller.contre_uno);
router.get("/seedefausse", [authJwt.verifyToken], controller.seeDefausse);

module.exports = router;
