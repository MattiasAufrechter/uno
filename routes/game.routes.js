const { authJwt } = require("../middleware");
const controller = require("../controllers/game.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/game/createParty",[authJwt.verifyToken], controller.createParty);
  app.get("/api/game/join/:partyId", [authJwt.verifyToken], controller.joinParty);
  app.get("/api/game/launchParty", [authJwt.verifyToken], controller.launchParty);
  app.get("/api/game/parties", controller.getAllParties);
  app.get("/api/game/cards", controller.getAllCards);
  app.get("/api/game/users", controller.getAllUsers);
  app.get("/api/game/userCards",[authJwt.verifyToken], controller.getAllCardsFromUser);
  app.get("/api/game/play/:id", [authJwt.verifyToken], controller.play);
  app.get("/api/game/play/:id?colorchoosed", [authJwt.verifyToken], controller.play);
  app.get("/api/game/pioche",[authJwt.verifyToken], controller.pioche);
  app.get("/api/game/uno", [authJwt.verifyToken], controller.uno);
  app.get("/api/game/contre_uno", [authJwt.verifyToken], controller.contre_uno);
  app.get("/api/game/seedefausse", [authJwt.verifyToken], controller.seeDefausse);
};
